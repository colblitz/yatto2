export const ServerVarsModel = {
  dMGScaleDown               :    0.1  ,
  helperInefficiency         :    0.025,
  helperInefficiencySlowDown :   34    ,
  helperUpgradeBase          :    1.08 ,
  evolveCostMultiplier       : 1000    ,
  petPassiveLevelGap         :    5    ,
  petPassiveLevelIncrement   :    0.01 ,
  petDamageIncLevel1         :   40    ,
  petDamageIncLevel2         :   80    ,
};